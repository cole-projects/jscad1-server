# jscad1-server

![npm](https://img.shields.io/npm/v/jscad1-server) ![GitLab last commit](https://img.shields.io/gitlab/last-commit/cole-projects/jscad1-server) ![GitLab issues](https://img.shields.io/gitlab/issues/open/cole-projects/jscad1-server) ![NPM Downloads](https://img.shields.io/npm/dt/jscad1-server) ![Twitter Follow](https://img.shields.io/twitter/follow/johnwebbcole?style=social)

This small utility will run the OpenJSCAD V1 website in a local server.

## Usage

```shell

npx jscad1-server

# or

npm i -g jscad1-server

jscad1-server

```

And open your browser to [http://localhost:3000](http://localhost:3000)
